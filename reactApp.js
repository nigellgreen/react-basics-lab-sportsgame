
// Default App component that all other components are rendered through
function App(props){
  return (
    <div>
      <h1>Welcome to the Football Game of the Year</h1>
      <h2>Welcome Teams</h2>
      <TeamComponent name="Greenbay Packers " logo={<img src="/img/greenbay.png" width="500" height="500" />} />
      <TeamComponent name="Seattle Seahawks " logo={<img src="/img/seattle.png" width="500" height="500"/>} />
    </div>
  )
}
class TeamComponent extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      score: 0,
      shotsTaken: 0,
    }
  }
  takeShot = () => {
    let score = this.state.score
    if (Math.random() > 0.5) {
      score += 7
    }
    this.setState((state, props) => ({
      shotsTaken: state.shotsTaken + 1,
      score
    }));
  }
  render() {
    return <div className="TeamComponent"> 
      {this.props.logo}{this.props.name}<h1>Stats:</h1>
      <span>Score {this.state.score}</span>
      <span> Shots Taken {this.state.shotsTaken}</span>
      <button onClick={this.takeShot}>{this.props.name} Turn</button>
      </div>
            }
          }
//Render the application
ReactDOM.render(
  <div>
    <App />
  </div>,
  document.getElementById('react')
);